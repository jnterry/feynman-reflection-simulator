package com.jamieterry.reflectionsimulator;

import java.util.ArrayList;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jamieterry.math.LineSegment;
import com.jamieterry.math.Vector2d;

public class FeynmanReflectionSim implements ApplicationListener {
	
	public class RayResult{
		Vector2d mirrorPoint;
		double length;
		double phase;
		double time;
	}
	
	//http://www.acamara.es/blog/2012/02/keep-screen-aspect-ratio-with-different-resolutions-using-libgdx/
	//the virtual screen size we render to
	private static final int VIRTUAL_WIDTH = 1024;
	private static final int VIRTUAL_HEIGHT = 768;
	private static final float ASPECT_RATIO = (float)VIRTUAL_WIDTH/(float)VIRTUAL_HEIGHT;
	
	//the current bottom and top of the graph
	private int mGraphLowerY = 20;
	private int mGraphUpperY = 300;
	
	private Rectangle mViewport;
	
	//array list of the current rays that have been calculated
	private ArrayList<RayResult> mRayResults = new ArrayList<RayResult>();
	
	//index of the mRayResults array which hold the ray with the shortest length
	private int mShortedRayIndex = 0;
		
	//index of the mRayResults array which hold the ray with the longest length
	private	int mLongestRayIndex = 0;
	
	//the renderer used to draw the shapes, lines, etc
	private ShapeRenderer mShapeRend;//cannot init  until "create()" is called or crash
	
	//the light source position
	private Vector2d mSource = new Vector2d(100,740);
	
	//light detector position
	private Vector2d mDetector = new Vector2d(924,740);
	
	//the start (left) end of the mirror
	private Vector2d mMirrorStart;
	
	//the end (right) end of the mirror 
	private Vector2d mMirrorEnd;
	
	//reference to the currently selected vector (ie: one being dragged)
	//null if none currently selected
	private Vector2d mSelectedVector = null;
	
	//the wavelength of light emitted by the source
	private double mWavelength = 100;
	
	//the speed of light emitted by the source
	private double mWaveSpeed = 1;
	
	//fps logger, libgdx feature which prints fps to system.out
	private FPSLogger mFpslogger = new FPSLogger();
	
	//sprite batch, needed for font renderering
	private SpriteBatch mSpriteBatch;
	
	//the font object used for rendering text
	BitmapFont mFont;
	
	//the "isTouched" staus of the previous frame
	private boolean mLastIsTouched;
	
	private FreeTypeFontGenerator mFontGen;
	
	
	@Override
	public void create() {
		mShapeRend = new ShapeRenderer();
		mSpriteBatch = new SpriteBatch();
		mFontGen = new FreeTypeFontGenerator(Gdx.files.internal("data/ARIAL.TTF"));
		mFont = mFontGen.generateFont(18);
		//mFont = new BitmapFont();
		this.resetScene();
		Gdx.gl.glEnable(GL10.GL_LINE_SMOOTH);
	}

	@Override
	public void dispose(){
		mSpriteBatch.dispose();
		mFont.dispose();
		mFontGen.dispose();
	}

	@Override
	public void render() { //libgdx only has a render function, there is no "update" or "input" functions, so everything is in here
		///////////////////////////////////////////////////////////////////////////////////
		//HANDLE INPUT
		boolean gotInput = false;
		if(Gdx.input.isKeyPressed(Keys.UP)){
			if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT)){
				this.mWavelength +=2;
			} else {
				this.mWavelength +=0.2;
			}
			gotInput = true;
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)){
			if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT)){
				this.mWavelength -=2;
			} else {
				this.mWavelength -=0.2;
			}
			gotInput = true;
		}
		if(Gdx.input.isKeyPressed(Keys.LEFT)){
			this.mWaveSpeed -=1;
			gotInput = true;
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)){
			this.mWaveSpeed +=1;
			gotInput = true;
		}
		if(Gdx.input.isTouched()){
			Vector3 touchPosGdx = new Vector3();
			touchPosGdx.set(Gdx.input.getX(), VIRTUAL_HEIGHT-Gdx.input.getY(), 0);
			
			//////////////////////////////////////////////////////////////////////////////////////
			//adjust touch position so that it matches the scaled resolution and translated by the black bars amount
			//note: why this works I don't know, it was done with trial and error while drawing dots for each transformation
			//don't touch ;)
			Vector2d touchPos = new Vector2d(touchPosGdx.x, touchPosGdx.y);
			touchPos.x -= mViewport.x;
			touchPos.y -= (VIRTUAL_HEIGHT-mViewport.y);
				
			touchPos.x *= VIRTUAL_WIDTH/mViewport.width;//scale down so it fits in the virtual width/height
			touchPos.y *= VIRTUAL_HEIGHT/mViewport.height;
			touchPos.y = -touchPos.y;
			touchPos.y = VIRTUAL_HEIGHT-touchPos.y;
			
			if(!mLastIsTouched){
				//then touch just started, if touching near a draggable vector, begin dragging it
				if(touchPos.getDistanceToSquared(mMirrorStart) < 100){
					this.mSelectedVector = mMirrorStart;
				} else if(touchPos.getDistanceToSquared(mMirrorEnd) < 100){
					this.mSelectedVector = mMirrorEnd;
				}
			}
			if(this.mSelectedVector != null){
				//then update the dragged vector
				this.mSelectedVector.set(touchPos);
				if(this.mSelectedVector == mMirrorStart || this.mSelectedVector == mMirrorEnd){
					//then do mirror point bounds checking
					if(this.mSelectedVector.x < 5){this.mSelectedVector.x = 5;}
					if(this.mSelectedVector.x > VIRTUAL_WIDTH-5){this.mSelectedVector.x = VIRTUAL_WIDTH-5;}
					if(this.mSelectedVector.y < 250){this.mSelectedVector.y=250;}
					if(this.mSelectedVector.y > VIRTUAL_HEIGHT-50){this.mSelectedVector.y=VIRTUAL_HEIGHT-50;}
				}
			}
			mLastIsTouched = true;
			gotInput = true;
		} else { //then not touched
			mLastIsTouched = false;
			mSelectedVector = null;
		}
		if(gotInput){
			///////////////////////////////////////////////////////////////////////////////////
			//UPDATE SCENE
			//no point updting rays if there was no change
			this.calcRays(30000);
		}
		
		
		mFpslogger.log();
		
		
		///////////////////////////////////////////////////////////////////////////////////
		//RENDER SCENE
		Gdx.gl.glClearColor(0,0,0,0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		
		mShapeRend.begin(ShapeType.Filled);
			//white background box
			mShapeRend.setColor(Color.WHITE);
			mShapeRend.rect(0,0,VIRTUAL_WIDTH,VIRTUAL_HEIGHT);
			
			//draw light source
			mShapeRend.setColor(Color.RED);
			mShapeRend.circle(mSource.x, mSource.y, 5);
			
			//draw light detector
			mShapeRend.setColor(Color.BLUE);
			mShapeRend.circle(mDetector.x, mDetector.y, 5);
			
			//draw mirror end points
			mShapeRend.setColor(new Color(0.4f, 0.4f, 0.4f, 1.0f));
			mShapeRend.circle(mMirrorStart.x, mMirrorStart.y, 7);
			mShapeRend.circle(mMirrorEnd.x, mMirrorEnd.y, 7);
		mShapeRend.end();
		
		mShapeRend.begin(ShapeType.Line);
			/////////////////////////////////////////////////
			//draw mirror
			Gdx.gl10.glLineWidth(5);
			mShapeRend.setColor(new Color(0.4f, 0.4f, 0.4f, 1.0f));
			mShapeRend.line(mMirrorStart.x, mMirrorStart.y, mMirrorEnd.x, mMirrorEnd.y);
			
			/////////////////////////////////////////////////
			//draw the rays and their phasors
			Gdx.gl10.glLineWidth(1);
			mShapeRend.setColor(Color.BLACK);
			//we only want to draw a certain number of rays, all of them is too many!
			double mirrorLength = this.mMirrorStart.getDistanceTo(this.mMirrorEnd);
			int rayInterval = (int) (mRayResults.size()/(mirrorLength/60));
			for(int i = 0; i < mRayResults.size(); i+=rayInterval){
				RayResult rr = mRayResults.get(i);
				//draw the rays from source to mirror to detector
				mShapeRend.line(rr.mirrorPoint.x,  rr.mirrorPoint.y, mSource.x, mSource.y);
				mShapeRend.line(rr.mirrorPoint.x,  rr.mirrorPoint.y, mDetector.x, mDetector.y);
			}
			//draw the shorted ray
			Gdx.gl10.glLineWidth(3);
			mShapeRend.setColor(Color.GREEN);
			mShapeRend.line(mRayResults.get(this.mShortedRayIndex).mirrorPoint.x, mRayResults.get(this.mShortedRayIndex).mirrorPoint.y, mSource.x, mSource.y);
			mShapeRend.line(mRayResults.get(this.mShortedRayIndex).mirrorPoint.x, mRayResults.get(this.mShortedRayIndex).mirrorPoint.y, mDetector.x, mDetector.y);
			
			Gdx.gl10.glLineWidth(1);
			int phasorInterval = rayInterval/10;
			//int phasorInterval = 1;
			for(int i = 0; i < mRayResults.size(); i+=phasorInterval){
				RayResult rr = mRayResults.get(i);
				//draw the ray's phasor just below the mirror point
				Vector2d phasorVec = new Vector2d(0,20);
				phasorVec = phasorVec.getRotated((float) rr.phase);
				mShapeRend.line(rr.mirrorPoint.x,  rr.mirrorPoint.y, rr.mirrorPoint.x+phasorVec.x, rr.mirrorPoint.y+phasorVec.y, Color.RED, Color.GREEN);
			}
			
			mSpriteBatch.begin();
				mFont.setColor(0.0f, 0.0f, 0.0f, 1.0f);
				mFont.draw(mSpriteBatch, "Wavelength: " + this.mWavelength + "(Up/Down Keys, Ctrl for fast mode)", 10, 760);
				mFont.draw(mSpriteBatch, "Wave Speed: " + this.mWaveSpeed  + "(Left/Right Keys)", 700, 760);
				mFont.draw(mSpriteBatch, "Ray Length Graph - Click & Drag Mirror Ends to Modify", this.mMirrorStart.x, this.mGraphUpperY+28);
			mSpriteBatch.end();
			
			
			
			/////////////////////////////////////////////////
			//draw graph
			float deltaRayLength = (float) (mRayResults.get(mLongestRayIndex).length-mRayResults.get(mShortedRayIndex).length);
			float shortestRayLength = (float) mRayResults.get(mShortedRayIndex).length;
			float graphHeight = mGraphUpperY-mGraphLowerY;
			float xMarkInterval = graphHeight/10.0f;
			//draw axies
			mGraphUpperY = (int) Math.min(mMirrorStart.y-60, mMirrorEnd.y-60);
			mShapeRend.setColor(new Color(0.7f, 0.7f, 0.7f, 0.1f));
				mShapeRend.line(mMirrorStart.x, mGraphLowerY, mMirrorStart.x, mGraphUpperY);
				for(float i = 0; i <= graphHeight+1; i+=xMarkInterval){
					mShapeRend.line(mMirrorStart.x, i+mGraphLowerY, mMirrorEnd.x, i+mGraphLowerY);
				}
			mShapeRend.setColor(Color.BLUE);
			//draw points
			for(int i = 1; i < mRayResults.size(); ++i){
				mShapeRend.line(mRayResults.get(i-1).mirrorPoint.x,
								(float) ((((mRayResults.get(i-1).length-shortestRayLength)/deltaRayLength)*graphHeight)+mGraphLowerY),
								mRayResults.get(i).mirrorPoint.x,
								(float) ((((mRayResults.get(i).length-shortestRayLength)/deltaRayLength)*graphHeight)+mGraphLowerY));
			}
		mShapeRend.end();
		//label y axis
		mSpriteBatch.begin();
			mFont.setColor(1.0f, 0.0f, 1.0f, 1.0f);
			for(double i = 0; i <= graphHeight+1; i+=xMarkInterval){
				mFont.draw(mSpriteBatch, ""+(int)(shortestRayLength+(i/graphHeight)*deltaRayLength), mMirrorStart.x+5, (float) (mGraphLowerY+i)+8);
			}
		mSpriteBatch.end();
	}

	@Override
	public void resize(int width, int height) {
		// calculate new viewport
        float aspectRatio = (float)width/(float)height;
        float scale = 1f;
        Vector2 crop = new Vector2(0f, 0f);
        if(aspectRatio > ASPECT_RATIO){
            scale = (float)height/(float)VIRTUAL_HEIGHT;
            crop.x = (width - VIRTUAL_WIDTH*scale)/2f;
        }
        else if(aspectRatio < ASPECT_RATIO){
            scale = (float)width/(float)VIRTUAL_WIDTH;
            crop.y = (height - VIRTUAL_HEIGHT*scale)/2f;
        }
        else{
            scale = (float)width/(float)VIRTUAL_WIDTH;
        }

        float w = (float)VIRTUAL_WIDTH*scale;
        float h = (float)VIRTUAL_HEIGHT*scale;
        mViewport = new Rectangle(crop.x, crop.y, w, h);
        Gdx.gl.glViewport((int) mViewport.x, (int) mViewport.y,(int) mViewport.width, (int) mViewport.height);
        System.out.println((int) mViewport.x + ", " + (int) mViewport.y + ", " + (int) mViewport.width + ", " + (int) mViewport.height);
        mFont = mFontGen.generateFont(Math.max((int) (18*scale),18));
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	public void calcRays(int pointCount){
		mRayResults.clear();//empty array of existing results
		mLongestRayIndex = 0;
		mShortedRayIndex = 0;
		
		//generate line segment for the mirror, and work out the spacing between points on the line
		double mirrorLength = mMirrorStart.getDistanceTo(this.mMirrorEnd);
		double pointSpacing = mirrorLength/(pointCount-1); //pointcount -1 as we want number of gaps, not number of points
		LineSegment mirrorSeg = LineSegment.createFromStartEnd(this.mMirrorStart, this.mMirrorEnd);
		
		//Calculate wave frequency
		//v = f * wavelength
		//v/wavelength = f
		double frequency = this.mWaveSpeed/this.mWavelength;
		
		//calculate rays for each point specified
		for(int i = 0; i < pointCount; ++i){
			RayResult rr = new RayResult();
			rr.mirrorPoint = mirrorSeg.getPointAtDistance((float) (pointSpacing*i));
			rr.length = (rr.mirrorPoint.getDistanceTo(this.mSource)) + (rr.mirrorPoint.getDistanceTo(mDetector));
			rr.time = rr.length/this.mWaveSpeed;
			rr.phase = rr.time*frequency;
			rr.phase -= (long)(rr.phase);//we don't care how many times it turned, we care about the fractional part of the next turn
			rr.phase *= 360; //convert from 0->1 to 0->360
			mRayResults.add(rr);
			
			//update longest and shortest array index if needed
			if(rr.length < mRayResults.get(mShortedRayIndex).length){
				mShortedRayIndex = i;
			}
			if(rr.length > mRayResults.get(mLongestRayIndex).length){
				mLongestRayIndex = i;
			}
		}
	}
	
	public void resetScene(){
		mMirrorStart = new Vector2d(100, 500);
		mMirrorEnd = new Vector2d(974, 500);
		this.calcRays(30000);//do an initial ray calc as it is usually only done on input
	}
}
